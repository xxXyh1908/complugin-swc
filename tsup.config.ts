// tsup.config.ts
import { defineConfig } from 'tsup'

export default defineConfig({
  // splitting: true,
  sourcemap: false,
  bundle: true,
  clean: true,
  dts: true,
  format: ['cjs', 'esm'],
  entryPoints: ['src/index.ts', 'src/nuxt.ts']
})
