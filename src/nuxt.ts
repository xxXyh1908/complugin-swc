import compluginSwc from './index'

export = function (this: any, options: compluginSwc.Options) {
  // install webpack plugin
  this.extendBuild((config: any) => {
    const plugins = config.plugins ?? (config.plugins = [])
    plugins.unshift(compluginSwc.webpack(options))
  })

  // install vite plugin
  this.nuxt.hook('vite:extend', async (vite: any) => {
    const plugins = vite.config.plugins ?? (vite.config.plugins = [])
    plugins.push(compluginSwc.vite(options))
  })
}
