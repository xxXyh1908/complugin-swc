[npm]: https://img.shields.io/npm/v/complugin-swc
[npm-url]: https://www.npmjs.com/package/complugin-swc
[size]: https://packagephobia.now.sh/badge?p=complugin-swc
[size-url]: https://packagephobia.now.sh/result?p=complugin-swc

# complugin-swc

[![npm][npm]][npm-url]
[![size][size]][size-url]

Complugin to compile bundles with the [`SWC`](https://swc.rs/).

## Install

### npm

```
$ npm install complugin-swc --save-dev
```

### Yarn

```
$ yarn add complugin-swc --dev
```

### pnpm

```
$ pnpm add complugin-swc --save-dev
```

## Options

- The configuration options of this plug-in are inherited from those of SWC. Please check the [configuration options of SWC](https://swc.rs/docs/configuration/compilation) for details.

```ts
interface Options extends SwcOptions {
  /**
   * A minimatch pattern, or array of patterns,
   * which specifies the files in the build the plugin should ignore.
   *
   * When relying on Swc configuration files you can only exclude additional files with this option,
   * you cannot override what you have configured for Swc itself.
   *
   */
  include?: (string | RegExp)[]

  /**
   * A minimatch pattern, or array of patterns, which specifies the files in the build the plugin should operate on.
   * When relying on Swc configuration files you cannot include files already excluded there.
   */
  exclude?: (string | RegExp)[]

  /**
   * Use SWC to transform your code.
   * @default true
   */
  enableTransform?: boolean

  /**
   * Compress your code with SWC
   * @default false
   */
  minify?: boolean
}
```

## Usage

##### Vite

```ts
// vite.config.ts
import Swc from 'complugin-swc'

export default {
  plugins: [
    Swc.vite({
      /* options */
    })
  ]
}
```

##### Rollup

```ts
// rollup.config.js
import Swc from 'complugin-swc'

export default {
  plugins: [
    Swc.rollup({
      /* options */
    })
  ]
}
```

##### Webpack

```ts
// webpack.config.js
const Swc = require('complugin-swc').default

module.exports = {
  plugins: [
    Swc.webpack({
      /* options */
    })
  ]
}
```

##### esbuild

```ts
// esbuild.config.js
import _esbuild from 'esbuild'
import { proxyEsbuild } from 'complugin'
import Swc from 'complugin-swc'

// Cannot be omitted
const esbuild = proxyEsbuild(_esbuild)

esbuild.build({
  plugins: [
    Swc.esbuild({
      /* options */
    })
  ]
})
```

## Give a ⭐️ if this project helped you!

## License

[MIT](./LICENSE) License © 2022 xxXyh1908
