import { CompluginInstance, createComplugin, proxyEsbuild } from 'complugin'
import _esbuild from 'esbuild'
import path from 'path'
import { rollup } from 'rollup'
import { build } from 'vite'
import webpack4 from 'webpack4'
import webpack5 from 'webpack5'
import compluginSwc from '../dist/index'

type checkerPluginOptions = {
  onSuccess: () => void
}

vi.mock('fs')
vi.mock('fs/promises')
vi.mock('fs-extra')
vi.mock('@complugin/checker', () => {
  return {
    get default() {
      return createComplugin<checkerPluginOptions>({
        name: '@complugin/checker',
        factory: (options, meta) => {
          return {
            resolveId(source) {
              if (source === 'virtual.ts') {
                return source
              }
            },
            load(id) {
              if (id === 'virtual.ts') {
                return `
                  export interface tsInterface {
                    msg: string,
                  }

                  export const msgObject: tsInterface = { msg: 'hello world' }
                `
              }
            },
            transform(code, id) {
              if (id === 'virtual.ts') {
                if (!code.includes('tsInterface') && code.includes('msgObject')) {
                  options?.onSuccess()
                }
              }
            },
            async generateBundle(bundle) {
              throw new Error()
            }
          }
        }
      })
    }
  }
})

const compluginTestInstance = compluginSwc({
  /* options */
})

describe('emit-asset', async () => {
  test('rollup', async () => {
    const compluginChecker = (
      await vi.importMock<{ default: CompluginInstance<checkerPluginOptions> }>('@complugin/checker')
    ).default
    let success = false

    try {
      await (
        await rollup({
          input: 'virtual.ts',
          plugins: [
            compluginTestInstance.rollup,
            compluginChecker.rollup({
              onSuccess: () => {
                success = true
              }
            })
          ]
        })
      ).generate({
        format: 'cjs',
        dir: path.join(__dirname, '__dist__'),
        sourcemap: 'inline'
      })
    } catch {}

    expect(success).toBeTruthy()
  })

  test('vite', async () => {
    const compluginChecker = (
      await vi.importMock<{ default: CompluginInstance<checkerPluginOptions> }>('@complugin/checker')
    ).default
    let success = false

    try {
      await build({
        base: '',
        build: {
          lib: { entry: 'virtual.ts', formats: ['cjs'] },
          rollupOptions: { input: 'virtual.ts' },
          outDir: path.join(__dirname, '__dist__'),
          minify: false,
          sourcemap: 'inline'
        },
        plugins: [
          compluginTestInstance.vite,
          compluginChecker.vite({
            onSuccess: () => {
              success = true
            }
          })
        ]
      })
    } catch {}

    expect(success).toBeTruthy()
  })

  test.each([
    [4, webpack4],
    [5, webpack5]
  ])('webpack%i', async (_, webpack) => {
    const compluginChecker = (
      await vi.importMock<{ default: CompluginInstance<checkerPluginOptions> }>('@complugin/checker')
    ).default
    let success = false

    try {
      await new Promise<void>((resolve, reject) => {
        webpack(
          {
            mode: 'production',
            entry: 'virtual.ts',
            output: {
              path: path.join(__dirname, '__dist__'),
              libraryTarget: 'commonjs'
            },
            optimization: {
              minimize: false
            },
            devtool: 'source-map',
            target: 'node',
            plugins: [
              compluginTestInstance.webpack,
              compluginChecker.webpack({
                onSuccess: () => {
                  success = true
                }
              })
            ]
          },
          (err: any, stats: any) => {
            console.log(err)
            resolve()
          }
        )
      })
    } catch (err) {
      console.log(err)
    }

    expect(success).toBeTruthy()
  })

  test('esbuild', async () => {
    const compluginChecker = (
      await vi.importMock<{ default: CompluginInstance<checkerPluginOptions> }>('@complugin/checker')
    ).default
    const esbuild = proxyEsbuild(_esbuild)
    let success = false

    try {
      await esbuild.build({
        entryPoints: ['virtual.ts'],
        plugins: [
          compluginTestInstance.esbuild,
          compluginChecker.esbuild({
            onSuccess: () => {
              success = true
            }
          })
        ],
        format: 'cjs',
        platform: 'node',
        assetNames: '[dir]/[name]',
        outdir: path.join(__dirname, '__dist__'),
        write: false,
        sourcemap: true
      })
    } catch {}

    expect(success).toBeTruthy()
  })
})
